#!/usr/local/bin/python
import os
import re
import urllib.request, urllib.error, urllib.parse
import hashlib
import json
import re
import boto3
def lambda_handler(event, context):
  URL = "https://ip-ranges.amazonaws.com/ip-ranges.json"
  r = urllib.request.urlopen(URL)
  ip_json = r.read()
  data=json.loads(ip_json)
  cdnlist=[]
  for prefix in data['prefixes']:
   if prefix['service'] == 'CLOUDFRONT':
    ip=prefix['ip_prefix']
    cdnlist.append(ip)
  print(cdnlist)
  print(len(cdnlist))
  sg=['sg-Xxxxxxx','sg-Xxxxxxx','sg-Xxxxxxx','sg-Xxxxxxx','sg-Xxxxxxx']
  sglength=[]
  ec2 = boto3.client('ec2',region_name='ap-southeast-1')
  sglist=[]
  for id in sg:
    print(id)
    oldlen=len(sglist)
    security_group = ec2.describe_security_groups(GroupIds=[id])
    try:
      for o in security_group["SecurityGroups"]:
        sg_ip=o['IpPermissions'][0]['IpRanges']
        for j in sg_ip:
          ig=j['CidrIp']
          sglist.append(ig)
      newlng=len(sglist)
      finallen=newlng-oldlen
      print(finallen)
      ipadd=50-finallen
      sglength.append(ipadd)
    except:
      sglength.append('50')
  print("sg IP list")
  print(sglist)
  print("++++++++++++++++++++++++++++++++++++++++++++++++++++")
  params_dict = {
        u'PrefixListIds': [],
        u'FromPort': 443,
        u'IpRanges': [],
        u'ToPort': 443,
        u'IpProtocol': 'tcp',
        u'UserIdGroupPairs': []
  }
  params_dict2 = {
        u'PrefixListIds': [],
        u'FromPort': 443,
        u'IpRanges': [],
        u'ToPort': 443,
        u'IpProtocol': 'tcp',
        u'UserIdGroupPairs': []
  }
  params_dict1 = {
        u'PrefixListIds': [],
        u'FromPort': 443,
        u'IpRanges': [],
        u'ToPort': 443,
        u'IpProtocol': 'tcp',
        u'UserIdGroupPairs': []
  }
  params_dict3 = {
        u'PrefixListIds': [],
        u'FromPort': 443,
        u'IpRanges': [],
        u'ToPort': 443,
        u'IpProtocol': 'tcp',
        u'UserIdGroupPairs': []
  }
  params_dict4 = {
        u'PrefixListIds': [],
        u'FromPort': 443,
        u'IpRanges': [],
        u'ToPort': 443,
        u'IpProtocol': 'tcp',
        u'UserIdGroupPairs': []
  }
  params_dict5 = {
        u'PrefixListIds': [],
        u'FromPort': 443,
        u'IpRanges': [],
        u'ToPort': 443,
        u'IpProtocol': 'tcp',
        u'UserIdGroupPairs': []
  }
  print("++++++++++++++++++++++++++++++++++++++++++++++++++++")
  print(sglength)
  print("++++++++++++++++++++++++++++++++++++++++++++++++++++")
  authorize_dict = params_dict.copy()
  authorize_dict2 = params_dict1.copy()
  authorize_dict3 = params_dict3.copy()
  authorize_dict4 = params_dict4.copy()
  authorize_dict5 = params_dict5.copy()
  print("++++++++++++++++++++++++++++New-list++++++++++++++++++++++++")
  newlist=[]
  for i in cdnlist:
    if i not in sglist:
     newlist.append(i)
  print(newlist)
  print("++++++++++++++++++++++++++++New-list++++++++++++++++++++++++")
  N=sglength[0]
  sg1=newlist[:int(N)]
  print(len(sg1))
  print(sg1)
  for i in sg1:
     authorize_dict['IpRanges'].append({u'CidrIp': i})
  print("++++++++++++++++++++++++++++sg1++++++++++++++++++++++++")
  sg2=[]
  for i in newlist:
    if i not in sg1:
      sg2.append(i)
  m=sglength[1]
  print(m)
  sgup2=sg2[:int(m)]
  print(sgup2)
  for i in sgup2:
    authorize_dict2['IpRanges'].append({u'CidrIp': i})

  print("++++++++++++++++++++++++++++sg2++++++++++++++++++++++++")
  sg3=[]
  for i in newlist:
    if i not in sgup2:
      if i not in sg1:
        sg3.append(i)
  m=sglength[2]
  print(m)
  sgup3=sg3[:int(m)]
  print(sgup3)
  for i in sgup3:
     authorize_dict3['IpRanges'].append({u'CidrIp': i})
  print("++++++++++++++++++++++++++++sg3++++++++++++++++++++++++")
  sg4=[]
  for i in newlist:
   if i not in sgup3:
     if i not in sgup2:
       if i not in sg1:
         sg4.append(i)
  m=sglength[3]
  print(m)
  sgup4=sg4[:int(m)]
  print(sgup4)
  for i in sgup4:
      authorize_dict4['IpRanges'].append({u'CidrIp': i})
  print("++++++++++++++++++++++++++++sg4++++++++++++++++++++++++")
  sg5=[]
  for i in newlist:
    if i not in sgup4:
      if i not in sgup3:
       if i not in sgup2:
         if i not in sg1:
           sg5.append(i)
  m=sglength[4]
  print(m)
  print(sg5)
  sgup5=sg5[:int(m)]
  for i in sgup5:
     authorize_dict5['IpRanges'].append({u'CidrIp': i})
  print("++++++++++++++++++++++++++++sg5++++++++++++++++++++++++")
  revoke_dict = params_dict2.copy()
  for i in sglist:
    if i not in cdnlist:
       revoke_dict['IpRanges'].append({u'CidrIp': i})
  print("the following new ip addresses will be added:")
  print(authorize_dict['IpRanges'])
  print(authorize_dict2['IpRanges'])
  print(authorize_dict3['IpRanges'])
  print(authorize_dict4['IpRanges'])
  print(authorize_dict5['IpRanges'])
  print("the following new ip addresses will be removed:")
  print(revoke_dict['IpRanges'])
  ec2 = boto3.client('ec2',region_name='ap-southeast-1')
  print("adding ip in all sg")
  ec2.authorize_security_group_ingress(GroupId='sg-Xxxxxxx',IpPermissions=[authorize_dict])
  ec2.authorize_security_group_ingress(GroupId='sg-Xxxxxxx',IpPermissions=[authorize_dict2])
  ec2.authorize_security_group_ingress(GroupId='sg-Xxxxxxx',IpPermissions=[authorize_dict3])
  ec2.authorize_security_group_ingress(GroupId='sg-Xxxxxxx',IpPermissions=[authorize_dict4])
  ec2.authorize_security_group_ingress(GroupId='sg-Xxxxxxx',IpPermissions=[authorize_dict5])
  print("removing ip in all dg")
  print(len(revoke_dict['IpRanges']))
  for id in sg:
     ec2.revoke_security_group_ingress(GroupId=id,IpPermissions=[revoke_dict])
  print("done")

event="shiv"
context="shiv"
lambda_handler(event, context)